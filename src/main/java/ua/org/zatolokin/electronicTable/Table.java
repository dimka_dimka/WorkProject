package ua.org.zatolokin.electronicTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by user on 06.08.2016.
 */
public class Table {

    private static Table table;
    private int hight;
    private int widht;
    private Cell[] cells;

    private Table() {
    }

    public static synchronized Table getInstance() { // синглтон
        if (table == null)
            table = new Table();
        return table;
    }

    public int getWidht() {
        return widht;
    }

    public int getHight() {
        return hight;
    }

    public Cell[] getCells() {
        return cells;
    }

    public void setHight(int hight) {
        this.hight = hight;
        if (widht != 0)
            setCellsNames();
    }

    public void setWidht(int widht) {
        this.widht = widht;
        if (hight != 0)
            setCellsNames();
    }

    private void setCellsNames() {
        cells = new Cell[hight * widht];
        int k = 0;
        StringBuilder sb = new StringBuilder();
        String[] names = giveNames(widht);
        for (int i = 1; i <= hight; i++) { // соединяем сгенерированную буквенную часть имён ячеек с цифровой
            for (int j = 0; j < widht; j++) {
                sb.append(names[j] + i);
                cells[k++] = new Cell(sb.toString());
                sb.delete(0, sb.length());
            }
        }
    }

    private String[] giveNames(int amount) { // метод для генерации буквенной части имён ячеек. Работает для любого количества ячеек
        char ch = 'A'; // последняя (либо единственная) буква в имени ячейки
        StringBuilder sb = new StringBuilder();
        ArrayList<Character> charPrefixs = new ArrayList() { // здесь будем хранить буквы имени текущей ячейки (кроме последней)
            @Override
            public String toString() { // переопределяем, что бы List записывался в строку с именем ячейки без лишних символов
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < this.size(); i++)
                    sb.append(this.get(i));
                return sb.toString();
            }
        };
        String[] names = new String[amount]; // здесь храним имена всех столбцов(буквенная часть имени ячейки)
        for (int i = 0; i < amount; i++) {
            sb.append(charPrefixs);
            sb.append(ch++);
            names[i] = sb.toString();
            sb.delete(0, sb.length());
            if (ch > 'Z') {
                ch = 'A';
                if (charPrefixs.size() == 0) {
                    charPrefixs.add('A');
                } else {
                    int lenght = 1; // вводим переменную для того, что бы перебирать столбцы в обратном порядке
                    while (lenght <= charPrefixs.size()) {
                        if (charPrefixs.get(charPrefixs.size() - lenght) >= 'Z') {
                            charPrefixs.set(charPrefixs.size() - lenght, 'A');
                            lenght++;
                        } else {
                            char elem = charPrefixs.get(charPrefixs.size() - lenght);
                            charPrefixs.set(charPrefixs.size() - lenght, ++elem);
                            break;
                        }
                    }
                    if (lenght > charPrefixs.size()) // если последнее имя состояло только из букв Z,
                        charPrefixs.add('A'); // то добавляем ещё один индекс
                }
            }
        }
        return names;
    }

    public void inputTable() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseSystemIn(System.in)))) {
            String tableSize = br.readLine();
            while (true) { // вводим высоту и ширину таблицы, пока не введём корректно
                try {
                    inputTableSizes(tableSize);
                    break;
                } catch (IncorrectInputException e) {
                    System.err.println(e.getMessage());
                    System.out.println("Пожалуйста, введите высоту и ширину таблицы заново: ");
                    tableSize = br.readLine();
                }
            }
            int count = 0;
            for (int i = 0; i < hight; i++) {
                String str = br.readLine();
                if (str == null) {
                    for (int j = 0; j < widht; j++) {
                        cells[count++].setText("' ");// пустые ячейки
                    }
                    continue;
                }
                String[] strings = str.split(" ");
                if (strings.length > widht) {
                    for (int j = 0; j < widht; j++) {
                        String text = strings[j];
                        cells[count++].setText(text);//отсекаем лишнее
                    }
                    continue;
                }
                for (String text : strings) {
                    cells[count++].setText(text);
                }
                if (strings.length < widht) {
                    for (int j = strings.length; j < widht; j++)
                        cells[count++].setText("' ");// пустые ячейки
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
       for (Cell cell : cells){ // вызываем автоматическое вычисление значения каждой ячейки
           cell.setValue();
       }
        System.out.println();
        printTable();
    }

    private void inputTableSizes(String text) throws IncorrectInputException {
        String[] str = text.split(" ");
        if (str == null || str.length != 2 || !isInt(str[0]) || !isInt(str[1]))
            throw new IncorrectInputException("Вы некорректно ввели размеры таблицы!");
        else {
            int hight = Integer.parseInt(str[0]);
            int width = Integer.parseInt(str[1]);
            if (hight <= 0 || width <= 0)
                throw new IncorrectInputException("Вы некорректно ввели размеры таблицы!");
            else {
                setHight(hight);
                setWidht(width);
            }
        }
    }

    private boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void printTable() {
        int count = 0;
        for (int i = 0; i < hight; i++) {
            for (int j = 0; j < widht; j++) {
                System.out.print(cells[count++].getValue() + "\t");
            }
            System.out.print("\n");
        }
    }
}
