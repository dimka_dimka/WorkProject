package ua.org.zatolokin.electronicTable;

/**
 * Created by user on 24.08.2016.
 */
public class IncorrectInputException extends Exception {

    public IncorrectInputException(String message){
        super(message);
    }
}
