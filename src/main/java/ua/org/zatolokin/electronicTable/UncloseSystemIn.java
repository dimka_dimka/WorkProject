package ua.org.zatolokin.electronicTable;

/**
 * Created by user on 24.08.2016.
 */
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class UncloseSystemIn extends FilterInputStream {

    protected UncloseSystemIn(InputStream in) {
        super(in);
    }

    @Override
    public void close() throws IOException {
        //NOP
    }

}
