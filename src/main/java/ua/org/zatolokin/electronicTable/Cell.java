package ua.org.zatolokin.electronicTable;

import java.util.ArrayList;

/**
 * Created by user on 06.08.2016.
 */
public class Cell {

    private String name;
    private String text;
    private Value value;

    private final String incorrect = "#incorrect";


    public Cell(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public Value getValue() {
        return value;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue() {
        if (text != null)
            value = parseText(text);
        else
            value = new Value(incorrect);
    }

    private Value parseText(String text) {
        if (isInt(text))
            return new Value(Integer.parseInt(text));
        char firstSymbol = text.charAt(0);
        if (firstSymbol == '\'') {
            String str = text.substring(1);
            return new Value(str);
        }
        if (firstSymbol == '=') {
            String str = text.substring(1);
            return parseExpression(str);
        }
        return new Value(incorrect);
    }

    private Value parseExpression(String str) {
        if (str == null)
            return new Value(incorrect);
        if (isInt(str))
            return new Value(Integer.parseInt(str));
        ArrayList<Character> arithmeticSigns = new ArrayList<>(); // для хранения арифметических знаков
        for (char ch : str.toCharArray()) {
            if (ch == '+' || ch == '-' || ch == '/' || ch == '*')
                arithmeticSigns.add(ch);
        }
        String[] devidedStr = str.split("[+-/*]");
        ArrayList<Integer> arrayForValue = new ArrayList<>();
        for (String st : devidedStr) {
            if (st == null)
                return new Value(incorrect);
            if (isInt(st)) {
                arrayForValue.add(Integer.parseInt(st));
            } else if (st.charAt(0) >= 'A' && st.charAt(0) <= 'Z') {
                for (int i = 0; i < Table.getInstance().getCells().length; i++) {
                    if (st.equals(Table.getInstance().getCells()[i].getName())) {
                        if (Table.getInstance().getCells()[i].getValue() == null) { // если в выражении присутствует ссылка на ячейку, которая ещё не посчитана
                            Value value = parseText(Table.getInstance().getCells()[i].getText()); // рекурсивно вызываем метод parseText для этой ячейки
                            if (isInt(value.toString())) {
                                arrayForValue.add(Integer.parseInt(value.toString()));
                                break;
                            }
                            else
                                return new Value(incorrect);
                        }
                        if (Table.getInstance().getCells()[i].getValue().equals(incorrect))
                            return new Value(incorrect);
                        if (isInt(Table.getInstance().getCells()[i].getValue().toString())) {
                            arrayForValue.add(Integer.parseInt(Table.getInstance().getCells()[i].getValue().toString()));
                            break;
                        } else
                            return new Value(incorrect);
                    }
                }
            } else
                return new Value(incorrect);
        }
        int currentValue = arrayForValue.get(0);
        for (int i = 1; i < arrayForValue.size(); i++) {
            switch (arithmeticSigns.get(i - 1)) {
                case '+':
                    currentValue += arrayForValue.get(i);
                    break;
                case '-':
                    currentValue -= arrayForValue.get(i);
                    break;
                case '*':
                    currentValue *= arrayForValue.get(i);
                    break;
                case '/':
                    currentValue /= arrayForValue.get(i);
                    break;
            }
        }
        return new Value(currentValue);
    }

    private boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private class Value<K> {

        private String type;
        private K value;

        public Value(K value) {
            this.value = value;
            type = this.value.getClass().getSimpleName();
        }

        public K getValue() {
            return value;
        }

        public void setValue(K value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }
}
