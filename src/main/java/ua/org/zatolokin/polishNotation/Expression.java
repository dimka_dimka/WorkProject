package ua.org.zatolokin.polishNotation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 15.12.2016.
 */
public class Expression {

    private String expr;
    private HashMap<String, String> inBrackets = new HashMap<>();

    public Expression(String str) {
        expr = str;
    }

    public String getExpr() {
        return expr;
    }

    public void setExpr(String expr) {
        this.expr = expr;
    }

    private boolean isNumber(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isNumber(char ch) {
        if (ch < '0' || ch > '9')
            return false;
        else
            return true;
    }


    private String substitutionBrackets(String str) throws IncorrectExpressionException { // помещаем все выражения в скобках в мапу
        StringBuilder sb = new StringBuilder();                                             // и заменяем их на выдуманные имена (ключи мапы)
        int count = 0;
        int mark = 0;
        int endBrackets = 0;
        boolean isInsideBrackets = false;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                count++;
                if (!isInsideBrackets) {
                    mark = i + 1;
                    sb.append(str.substring(endBrackets, mark - 1));
                }
                isInsideBrackets = true;
            }
            if (str.charAt(i) == ')')
                count--;
            if (count < 0)
                throw new IncorrectExpressionException("Вы некорректно расставили скобки!");
            if (count == 0) {
                if (isInsideBrackets) {
                    String s = Name.getName();
                    inBrackets.put(s, str.substring(mark, i));
                    sb.append(s);
                    isInsideBrackets = false;
                    endBrackets = i + 1;
                }
            }
        }
        if (count != 0)
            throw new IncorrectExpressionException("Вы некорректно расставили скобки!");
        if (sb.length() == 0)
            sb.append(str);
        if (endBrackets != 0 && endBrackets < str.length() - 1)
            sb.append(str.substring(endBrackets, str.length()));
        return sb.toString();
    }

    private int getPriority(String ch) {
        int result = 0;
        switch (ch) {
            case "+":
            case "-":
                result = 1;
                break;
            case "*":
            case "/":
                result = 2;
                break;
            case "^":
                result = 3;
                break;
        }
        return result;
    }

    private ArrayList<String> reverse(ArrayList<String> arr) {
        ArrayList<String> rev = new ArrayList<>();
        for (int i = arr.size() - 1; i >= 0; i--) {
            rev.add(arr.get(i));
        }
        return rev;
    }

    private String evaluateExpression(String str) { // преобразовываем обычное выражение (без скобок) в польскую запись
        ArrayList<String> symbols = new ArrayList<>();
        ArrayList<String> members = new ArrayList<>();
        ArrayList<String> expression = new ArrayList() {
            @Override
            public String toString() {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < this.size(); i++)
                    sb.append(this.get(i));
                return sb.toString();
            }
        };
        int mark = 0;
        int markForSymbols = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '+' || str.charAt(i) == '-' || str.charAt(i) == '*' || str.charAt(i) == '/'
                    || str.charAt(i) == '^' || i == str.length() - 1) {
                if (i == str.length() - 1) {
                    if (str.charAt(i) == '+' || str.charAt(i) == '-' || str.charAt(i) == '*' || str.charAt(i) == '/'
                            || str.charAt(i) == '^') {
                        return str; // исключаем возможность повторного преобразования строки
                    }
                    members.add(str.substring(mark, i + 1));
                    expression.addAll(members);
                    expression.addAll(reverse(symbols));
                    members.clear();
                    symbols.clear();
                    break;
                } else {
                    if (i == mark){
                        return str; // исключаем возможность повторного преобразования строки
                    }
                    members.add(str.substring(mark, i));
                }
                mark = i + 1;
                if (symbols.size() > 0) {
                    if (getPriority(str.substring(i, i + 1)) <= getPriority(symbols.get(symbols.size() - 1))) {
                        expression.addAll(members);
                        if (symbols.size() > 1) {
                            for (int j = symbols.size() - 2; j >= 0; j--) {
                                if (getPriority(str.substring(i, i + 1)) > getPriority(symbols.get(j))) {
                                    markForSymbols = j + 1;
                                    break;
                                }
                            }
                            ArrayList<String> prioritySymbols = new ArrayList<>(); // list для хранения части symbols, которые приоритетней текущего знака
                            for (int a = markForSymbols; a < symbols.size(); a++)
                                prioritySymbols.add(symbols.get(a));
                            expression.addAll(reverse(prioritySymbols));// добавляем приоритетные символы
                            members.clear();
                            symbols.removeAll(prioritySymbols); // а остальные оставляем в листе
                            symbols.add(str.substring(i, i + 1));
                        } else {
                            expression.addAll(reverse(symbols));
                            members.clear();
                            symbols.clear();
                            symbols.add(str.substring(i, i + 1));
                        }
                    } else {
                        symbols.add(str.substring(i, i + 1));
                    }
                } else {
                    symbols.add(str.substring(i, i + 1));
                }
            }
        }
        return expression.toString();
    }

    private void removeAllBrackets() throws IncorrectExpressionException { // проходимся по значениям в мапе и преобразуем их
        String value;
        String key;
        int count;
        for (int i = 0; i < inBrackets.size(); i++) {
            key = "#" + i + "#";
            value = inBrackets.get(key);
            if (value.contains("(")) {
                inBrackets.put(key, substitutionBrackets(value)); // заменяем выражение в мапе со скобками на обработанное
            }
        }
        do {
            count = 0;
            for (int i = 0; i < inBrackets.size(); i++) {
                key = "#" + i + "#";
                value = inBrackets.get(key);
                if (value.contains("#")) {
                    inBrackets.put(key, finallyDecide(value)); // заменяем выражение в мапе с '#' на обработанное
                    count++;
                }
            }
        } while (count != 0);
    }

    private String finallyDecide(String str) { // преобразуем неготовые выражения в мапе и вставляем в итоговую строку готовые выражения
        StringBuilder sb = new StringBuilder();
        String st = evaluateExpression(str);
        for (int i = 0; i < inBrackets.size(); i++) {
            inBrackets.put("#" + i + "#", evaluateExpression(inBrackets.get("#" + i + "#")));
        }
        int mark = 0;
        int nextIndex = 0;
        for (int i = 0; i < st.length(); i++) {
            if (st.charAt(i) == '#') { // находим подстроки, взятые с обеих сторон в '#' и заменяем их на соответствующие значения из мапы
                sb.append(st.substring(mark, i));
                nextIndex = st.indexOf("#", i + 1);
                mark = nextIndex + 1;
                sb.append(inBrackets.get(st.substring(i, nextIndex + 1)));
                i = nextIndex;
            }
        }
        if (nextIndex < st.length() - 1) {
            sb.append(st.substring(mark, st.length()));
        }
        return sb.toString();
    }

    public String compileExpression() throws IncorrectExpressionException { // собираем конечное выражение
        String str = substitutionBrackets(expr);
        if (inBrackets.size() == 0)
            return evaluateExpression(str);
        else {
            removeAllBrackets();
            return finallyDecide(str);
        }
    }
}
