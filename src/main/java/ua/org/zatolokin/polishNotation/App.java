package ua.org.zatolokin.polishNotation;

/**
 * Created by user on 20.12.2016.
 */
public class App {

    public static void main(String[] args) {

        Expression ex = new Expression("a+b-(c+d)*e+f*(k-(t+r)*w^y)");

        try {
            //System.out.println(Expression.compileExpression("a+v*(g+d*c)-g+(G-y)*(h+(n-c*y))"));
            //System.out.println(Expression.compileExpression("a+v*(g+d*c)-g+S^3/(G-y)^2^5*(h+(n-c*y))"));
            //System.out.println(Expression.compileExpression("a+b/c/d/e*f"));
            //System.out.println(Expression.compileExpression("a+(b-c)*k"));
            System.out.println(ex.compileExpression());
        } catch (IncorrectExpressionException e) {
            e.printStackTrace();
        }
    }
}