package ua.org.zatolokin.polishNotation;

/**
 * Created by user on 20.12.2016.
 */
public class IncorrectExpressionException extends Exception {

    public IncorrectExpressionException(String message) {
        super(message);
    }
}
