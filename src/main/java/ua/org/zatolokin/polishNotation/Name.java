package ua.org.zatolokin.polishNotation;

/**
 * Created by user on 03.01.2017.
 */
public class Name {

    private static int count = 0;

    private Name(){
    }

    public static String getName(){
        return "#" + count++ + "#";
    }
}
